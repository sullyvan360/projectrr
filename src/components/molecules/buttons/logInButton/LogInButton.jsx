//----- MODULES
import React, { Component, useState } from "react";
import Axios from "axios";
import { Form, Modal } from "react-bootstrap";

//----- MULTIMEDIAS
import LogoModal from "../../../atoms/img/logo/logoResponsive.png";

class LogIn extends Component {
    constructor(props){
        super(props);
        this.state={
            //*----- STATES BdD -----*//
            membres:           [],

            //*----- STATES LOCALES -----*//
            pseudonymeLogIn:   "",
            pseudonymeSignIn:  "",
            nom:               "",
            prenom:            "",
            dateDeNaissance:   "",
            eMail:             "",
            motDePasseLogIn:   "",
            motDePasseSignIn:  "",
            confirmMotDePasse: ""
        }
    }
    render() {
        console.log("datas", this.state)
        return(
            <div>
                <LogInButton datas={this.state}/>
            </div>
        )
    }
}

function LogInButton(props){
    console.log("propsLogInButton", props);
    /*VARIABLES*/
    const [indexTabs , setIndexTabs]                = useState(2);
    const [pseudonymeSignIn, setPseudonymeSignIn]   = useState('');
    const [pseudonymeLogIn, setPseudonymeLogIn]     = useState('');
    const [nom, setNom]                             = useState('');
    const [prenom, setPrenom]                       = useState('');
    const [dateDeNaissance, setDateDeNaissance]     = useState('');
    const [eMail, setEmail]                         = useState('');
    const [motDePasseSignIn, setMotDePasseSignIn]   = useState('');
    const [motDePasseLogIn, setMotDePasseLogIn]     = useState('');
    const [confirmMotDePasse, setConfirmMotDePasse] = useState('');
    const [membres, setMembres]                     = useState([]);

    /*FONCTIONS*/
    const indexTabsContent = (index) => {
        setIndexTabs(index);
    }
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const onSubmit = (data, e) => console.log(data, e);
    const onError = (errors, e) => console.log(errors, e);

    const handleSubmitLogIn = (event) => {
        event.preventDefault();
        console.log("event :", event);
        const getMembres = Axios({
            method:       'get',
            url:          `https://ressourcesrelationnelles.space//items/membres`,
            responseType: 'json',
            }).then(response => {
            setMembres([...response.data.data]);
            });
            handleClose();
            console.log("getMembres", getMembres);
    }

    const handleSubmitSignIn = (event) => {
        event.preventDefault();
        Axios({
            method:       'post',
            url:          `https://ressourcesrelationnelles.space//items/rr_membres`,
            data: {
                NOM_MEMBRE:             nom,
                PRENOM_MEMBRE:          prenom,
                NOM_UTILISATEUR_MEMBRE: pseudonymeSignIn,
                PASSWORD_MEMBRE:        motDePasseSignIn,
                DATE_NAISSANCE:         dateDeNaissance,
                MAIL_MEMBRE:            eMail
            }
        }).then(function(response){
            console.log(response);
        }).catch(function(erreur){
            console.log(erreur);
        })
    }


    const handleChangePseudonymeLogIn = (event) => {
        setPseudonymeLogIn(event.target.value);
        console.log("pseudonyme", event.target.value);
    }
    const handleChangePseudonymeSignIn = (event) => {
        setPseudonymeSignIn(event.target.value);
        console.log("Pseudonyme", event.target.value);
    }
    const handleChangeNom = (event) => {
        setNom(event.target.value);
        console.log("Nom", event.target.value);
    }
    const handleChangePrenom = (event) => {
        setPrenom(event.target.value);
        console.log("Prenom", event.target.value);
    }
    const handleChangeDateDeNaissance = (event) => {
        setDateDeNaissance(event.target.value);
        console.log("DateDeNaissance", event.target.value);
    }
    const handleChangeEmail = (event) => {
        setEmail(event.target.value);
        console.log("Email", event.target.value);
    }
    const handleChangeMotDePasseLogIn = (event) => {
        setMotDePasseLogIn(event.target.value);
        console.log("MotDePasse", event.target.value);
    }
    const handleChangeMotDePasseSignIn = (event) => {
        setMotDePasseSignIn(event.target.value);
        console.log("ChangeMotDePasse", event.target.value);
    }
    const handleChangeConfirmMotDePasse = (event) => {
        setConfirmMotDePasse(event.target.value);
        console.log("ConfirmMotDePasse", event.target.value);
    }

    return (
        <div className="parentButtonPositionInscriptionTopNavBar">
            <button className="leftNavBar-bouton" onClick={handleShow}>Se connecter</button>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <img src={LogoModal} alt="Logo ressource relationelles" className="logoModal"/>
                    <Modal.Title className={indexTabs === 1 ? 'activeContent  centerTitle' : 'content'}>S'inscrire à <br/>Ressources Relationnelles</Modal.Title>
                    <Modal.Title className={indexTabs === 2 ? 'activeContent  centerTitle' : 'content'}>Se connecter à <br/>Ressources Relationnelles</Modal.Title>
                    <div className="positionBoutonInscriptionConnexion" >
                        <a href="#inscription" className={indexTabs === 1 ? 'activeText col-6' : 'col-6 btnTopNavBarInsCon'} onClick={() =>  indexTabsContent(1) }>S'inscrire </a>
                        <a href="#connexion" className={indexTabs === 2 ? 'activeText col-6' : 'col-6 btnTopNavBarInsCon'} onClick={() =>  indexTabsContent(2) }>Se connecter </a>
                    </div>
                </Modal.Header>

                <Modal.Body className={indexTabs === 1 ? 'activeContent' : 'content'}>
                    <Form onSubmit={handleSubmitSignIn}>
                        <Form.Group controlId="formInscriptionPseudo">
                            <Form.Label>
                                Pseudonyme
                            </Form.Label>
                            <Form.Control  type="text" placeholder="Entrez un pseudonyme" onChange={handleChangePseudonymeSignIn}/>
                        </Form.Group>

                        <Form.Group  controlId="formInscriptionName">
                            <Form.Label >
                                Nom
                            </Form.Label>
                            <Form.Control type="text" placeholder="Entrez votre nom" onChange={handleChangeNom}/>
                        </Form.Group>
                        <Form.Group  controlId="formInscriptionPrenom">
                            <Form.Label >
                                Prénom
                            </Form.Label>
                            <Form.Control type="text" placeholder="Entrez votre prénom" onChange={handleChangePrenom}/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Date de naissance</Form.Label>
                            <Form.Control type="date" onChange={handleChangeDateDeNaissance}/>
                        </Form.Group>
                        <Form.Group controlId="formInscriptionEmail">
                            <Form.Label >
                                E-mail
                            </Form.Label>
                            <Form.Control type="email" placeholder="Entrez une adresse mail valide" onChange={handleChangeEmail}/>
                        </Form.Group>
                        <Form.Group controlId="formInscriptionPassword">
                            <Form.Label >
                                Mot de passe
                            </Form.Label>
                            <Form.Control type="password" placeholder="Choissisez un mot de passe" onChange={handleChangeMotDePasseSignIn}/>
                            <Form.Text id="passwordHelpBlock" muted>
                            Votre mot de passe doit comporter entre 8 et 20 caractères, contenir des lettres et des chiffres, et ne doit pas contenir d'espaces, de caractères spéciaux ou d'emoji.
                            </Form.Text>
                        </Form.Group>
                        <Form.Group  controlId="formInscriptionPasswordConfirm">
                            <Form.Label >
                                Confirmez votre mot de passe
                            </Form.Label>
                            <Form.Control type="password" placeholder="Confirmer un mot de passe" onChange={handleChangeConfirmMotDePasse}/>
                        </Form.Group>
                        <p className="text-privacy-conditions">
                            En cliquant sur s'inscrire, vous reconnaissez avoir lu et approuvé les
                            <a href="/ressources_relationnelles/conditions">Conditions d'utilisations</a>
                            et la
                            <a href="/ressources_relationnelles/politicalCookies">Politique de confidentialité.</a>
                        </p>
                        <input type="submit" className={indexTabs === 1 ? 'activeContent buttonInscription' : 'content buttonInscription'} value="S'inscrire"/>
                        <input type="submit" className={indexTabs === 2 ? 'activeContent buttonInscription' : 'content buttonInscription'} value="Se connecter"/>
                    </Form>
                </Modal.Body>

                <Modal.Body className={indexTabs === 2 ? 'activeContent' : 'content'}>
                    <Form className="fullWidthForm" onSubmit={handleSubmitLogIn}>
                        <Form.Group  controlId="formConnexionID">
                            <Form.Label >
                                Identifiant
                            </Form.Label>
                            <Form.Control type="text" onChange={handleChangePseudonymeLogIn}/>
                        </Form.Group>

                        <Form.Group  controlId="formConnexionPassword">
                            <Form.Label >
                                Mot de passe
                            </Form.Label>
                            <Form.Control type="password" onChange={handleChangeMotDePasseLogIn}/>
                            <Form.Text>Mot de passe oublié ?</Form.Text>
                        </Form.Group>
                        <input type="submit" className={indexTabs === 1 ? 'activeContent buttonInscription' : 'content buttonInscription'} value="S'inscrire"/>
                        <input type="submit" className={indexTabs === 2 ? 'activeContent buttonInscription' : 'content buttonInscription'} value="Se connecter"/>
                    </Form>
                </Modal.Body>
            </Modal>
        </div>
    )
}
export default LogIn;
