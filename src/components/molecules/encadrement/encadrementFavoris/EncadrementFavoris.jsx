//MODULES
import React, { Fragment, Component } from 'react';

//COMPONENTS
import BackButtonMyAccount from '../../../atoms/img/monCompte/backButton.png';

//STYLES
import './encadrementFavoris.css';

class EncadrementFavoris extends Component {
   constructor(props){
      super(props);
      this.state = {
         ressources : []
      }
      console.log('propsFavoris', props);
   }
   render(){
      return(
         <Fragment>
            <div className="encadrementFavoris">
               <h1>{this.props.title}</h1>
               <div>
                  <a><img src={BackButtonMyAccount}/></a>
               </div>
            </div>
         </Fragment>
      )
   }

}
export default EncadrementFavoris;