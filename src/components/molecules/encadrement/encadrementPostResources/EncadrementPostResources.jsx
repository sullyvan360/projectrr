//MODULES
import React, { Fragment, Component } from 'react';
import Axios from 'axios';
import { Link } from 'react-router-dom';

//COMPONENTS
import ResourceCard from '../../../organismes/ressourceCard/ResourceCard';
import BackButtonMyAccount from '../../../atoms/img/monCompte/backButton.png';

//STYLES
import './encadrementPostResources.css';

class EncadrementPostResources extends Component {
   constructor(props){
      super(props);
      this.state = {
         ressources : [],
         membres   : []
      }
      console.log('propsPostResources', props);
   }
   getResources = () => {
      Axios({
      method:       'get',
      url:          `https://ressourcesrelationnelles.space//items/rr_ressources?limit=5`,
      responseType: 'json'
    }).then(response => {
        this.setState({ ressources: [...this.state.ressources, ...response.data.data] });
    })
   }
   render(){
      return(
         <Fragment>
            <div className="encadrementPostResources">
               <h1>{this.props.title}</h1>
               <div className="div_ResourceContainer">
                  <ul>
                  {this.state.ressources.map((ressource, indexRessource) =>
                     <Link key={indexRessource} to="/ressources_relationnelles/moncompte/aperçu">
                        <ResourceCard tableRessources={ressource} tableMembres={this.state.membres} />
                     </Link>
                  )}
                  </ul>
               </div>
               <div className="btnEncadrement">
                  <a><img src={BackButtonMyAccount}/></a>
               </div>
            </div>
         </Fragment>
      )
   }
}
export default EncadrementPostResources;