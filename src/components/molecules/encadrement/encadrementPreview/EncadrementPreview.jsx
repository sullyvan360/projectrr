//MODULES
import React, { Fragment, Component } from 'react';

//COMPONENTS
import Birdhday from '../../../atoms/img/monCompte/pinkIcons/birthday.png';
import Geo from '../../../atoms/img/monCompte/pinkIcons/geo.png';
import DateSignIn from '../../../atoms/img/monCompte/pinkIcons/datesignin.png';
import Tel from '../../../atoms/img/monCompte/pinkIcons/tel.png';
import Facebook from '../../../atoms/img/monCompte/pinkIcons/facebook.png';
import Instagram from '../../../atoms/img/monCompte/pinkIcons/instagram.png';
import Twitter from '../../../atoms/img/monCompte/pinkIcons/twitter.png';
import Tiktok from '../../../atoms/img/monCompte/pinkIcons/tiktok.png';
import Youtube from '../../../atoms/img/monCompte/pinkIcons/youtube.png';
import Twitch from '../../../atoms/img/monCompte/pinkIcons/twitch.png';
import Discord from '../../../atoms/img/monCompte/pinkIcons/discord.png';
import Linkedin from '../../../atoms/img/monCompte/pinkIcons/linkedin.png';
import BackButtonMyAccount from '../../../atoms/img/monCompte/backButton.png';

//STYLES
import './encadrementPreview.css';

class EncadrementPreview extends Component {
   constructor(props){
      super(props);
      this.state = {

      }
      console.log('propsEncadrement', props);
   }
   render(){
      return(
         <Fragment>
            <div className="encadrementPreview">
               <h1>{this.props.title}</h1>
               <div className="premièresInfos">
                  <div>
                     <img src={Birdhday} />
                     <span>Mar 30</span>
                  </div>
                  <div>
                     <img src={Geo} />
                     <span>Châteauroux</span>
                  </div>
                  <div>
                     <img src={DateSignIn} />
                     <span>Inscrit depuis le 11/03/2021</span>
                  </div>
                  <div>
                     <img src={Tel} />
                     <span>06 69 96 66 99</span>
                  </div>
               </div>
               <div className="mesReseauxSociaux">
                  <div>MES RESEAUX SOCIAUX</div>
                  <div>
                     <img src={Facebook} />
                     <img src={Instagram} />
                     <img src={Twitter} />
                     <img src={Tiktok} />
                     <img src={Youtube} />
                     <img src={Twitch} />
                     <img src={Discord} />
                     <img src={Linkedin} />
                  </div>
               </div>
               <div className="maBiographie">
                  <span>MA BIOGRAPHIE</span>
                  <p>
                  Horum adventum praedocti speculationibus fidis rectores militum tessera data sollemni armatos omnes celeri eduxere procursu et agiliter praeterito Calycadni fluminis ponte, cuius undarum magnitudo murorum adluit turres, in speciem locavere pugnandi. neque tamen exiluit quisquam nec permissus est congredi. formidabatur enim flagrans vesania manus et superior numero et ruitura sine respectu salutis in ferrum.

Nihil morati post haec militares avidi saepe turbarum adorti sunt Montium primum, qui divertebat in proximo, levi corpore senem atque morbosum, et hirsutis resticulis cruribus eius innexis divaricaturn sine spiramento ullo ad usque praetorium traxere praefecti.

Post hanc adclinis Libano monti Phoenice, regio plena gratiarum et venustatis, urbibus decorata magnis et pulchris; in quibus amoenitate celebritateque nominum Tyros excellit, Sidon et Berytus isdemque pares Emissa et Damascus saeculis condita priscis.
                  </p>
               </div>
               <div className="btnEncadrement">
                    <a><img src={BackButtonMyAccount} /></a>
                </div>
            </div>
         </Fragment>
      )
   }

}
export default EncadrementPreview;