//----- MODULES
import React, { Component } from 'react';

//----- MULTIMEDIA
import Image1 from '../template1/images/image1.png';

class Template1 extends Component {
    render(){
        return(
            <section>
                <div className="line_1">
                    <img src={Image1} alt="" />
                </div>
            </section>
        )
    }
}
export default Template1;