//*----- COMPOSANTS -----//
import Logo from '../../atoms/img/logo/logoResponsive.png'

//*----- Styles -----//
import './spinner.css'

const Spinner = (props) => {
    return (
        <div className={props.classSpinner}>
            <div id="parentLoadingData" className="parent-loader">
                <div className="loader"></div>
                <div className="loader-picture">
                    <img src={Logo} alt="logo ressources relationnelles" />
                </div>
            </div>
        </div>
    )
}

export default Spinner;