import React from "react";
import { Card } from "react-bootstrap";
import { Step } from "./Wizard";
import heartIcon from '../../atoms/icons/heartIcon.png';
import commentIcon from '../../atoms/icons/commentIcon.png';
import shareIcon from '../../atoms/icons/shareIcon.png';
import starIcon from '../../atoms/icons/starIcon.png';


export default class cardRessource extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            date: new Date().toLocaleDateString(),
        };
    }

    render() {
        return (
            <div id="ressourceCardAddRessources" className="content-wizard card-ressource-wizard">
                <Step>
                    <article className="divided-content">
                        <div id="resourceCard">
                            <div id="lbl_publicationResourceCard" className="col-12 lbl_publicationResourceCard">
                                <span id="span_publicationResourceCard" className="span_publicationResourceCard">publié le : <strong>{this.state.date}</strong></span>
                            </div>
                            <div className="cardRessource">
                                <div className="cardBody">
                                    <Card.Body style={{ padding: 10, backgroundColor: "#F7F7F7", borderTopRightRadius: 18 }}>
                                        <div className="titleResourceCard">
                                            <Card.Title><strong id="cardTitleRessource" className="tailleTitreCardRessources">{this.state.titre}</strong></Card.Title>
                                        </div>
                                        <div className="contentPictureResourceCard col-12">
                                            <Card.Img
                                                id="imageURLCard"
                                                style={{ alignItems: 'center' }}
                                                src=" "
                                                alt=""
                                                width="290px"
                                                height="160px"
                                            />
                                        </div>
                                        <div className="colorCardBadge">
                                            <span id="categoryValueCard" className="badgeRessourceCard" variant="primary"></span>
                                            <span id="relationsValueCard" className="badgeRessourceCard" variant="secondary"></span>
                                            <span id="ressourceValueCard" className="badgeRessourceCard" variant="secondary"></span>
                                        </div>

                                        <div className="ProfilPictureResourceCard">
                                            <Card.Img
                                                style={{ width: '40px' }}
                                            />
                                            <span className="span_PseudonymeMembreResourceCard"><strong></strong></span>
                                            <span className="span_ProfessionMembreResourceCard"></span>
                                        </div>
                                    </Card.Body>
                                </div>
                                <div id="contentTextArea" className="ressource-content-hide">

                                </div>
                                <Card.Footer className="FooterResourceCard">
                                    <div className="row iconResourceCard">
                                        <span><IconStatResourceCard name={heartIcon} /></span>
                                        <span><IconStatResourceCard name={commentIcon} /></span>
                                        <span><IconStatResourceCard name={shareIcon} /></span>
                                        <span><IconStatResourceCard name={starIcon} /></span>
                                    </div>
                                </Card.Footer>
                            </div>
                        </div>
                    </article>
                </Step>
            </div>
        )
    }
}

function IconStatResourceCard(props) {
    return (
        <div>
            <Card.Img src={props.name} />
            <div className="ligneStat">
                <span className="span_numberIconResourceCard">{props.number}</span>
            </div>
        </div>
    )
}