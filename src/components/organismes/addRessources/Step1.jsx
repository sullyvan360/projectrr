import React from "react";
import { Step } from "./Wizard"


export default class Step1 extends React.Component {

    render() {
        return (
            <div className="col-12 content-wizard">
                <Step>
                    <h1 className="title-add-ressource text-center">Ajouter une ressource</h1>
                    <article>
                        <p>Bienvenue dans la fonction d’ajout de ressource.</p>
                        <p>
                            Vous pouvez publier du contenu informatif ou intéractif
                            en téléversant tout type de multimédia depuis votre ordinateur ou via des URLs.
                            Attention toutefois à la limite de tailles prises en charge par nos services.
                        </p>
                        <p>
                            Pour rappel, il est interdit de [...]
                        </p>
                        <p>Nous vous suggérons de lire les conditions d’utilisations pour plus de précisions
                            avant votre publication.
                        </p>
                        <p>
                            Pour tout problème rencontré lors des différentes étapes ci-après,
                            veuillez le reporter dans la section Signaler un problème situé dans les paramètres
                            pour nous aider à l’amélioration de la plateforme.
                        </p>
                        <p>
                            Nous vous souhaitons une agréable expérience.
                        </p>
                    </article>
                </Step>
            </div>
        )
    }
}