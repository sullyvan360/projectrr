import React from "react";
import { Form } from "react-bootstrap";
import { Step } from "./Wizard";

export default class Step2 extends React.Component {
    constructor(props) {
        super(props);

        this.handleInputKeyUp = this.onKeyupInput.bind(this);
    }

    onKeyupInput = (evt) => {
        document.getElementById("cardTitleRessource").textContent = evt.target.value;
    }

    render() {
        return (
            <div className="col-12 content-wizard">
                <Step>
                    <h1 className="title-add-ressource text-center">Ajouter une ressource</h1>
                    <article className="divided-content">
                        <Form className="col-6">
                            <h2 className="title-left-content">Titre de la ressource</h2>
                            <Form.Group className="mb-3">
                                <Form.Control
                                    type="text"
                                    id="formBasicEmail"
                                    onKeyUp={this.handleInputKeyUp}
                                />
                            </Form.Group>
                            <p>
                                Le titre de la ressource est utilisé comme mot clé pour la fonction de recherche.
                            </p>
                            <p>
                                Igitur scirent nostris diffuso magnis propinqua populationum et intactam conmunitam diffuso populationum undique scirent timore scirent diu stataria latrones stataria et praesidiis conmunitam se Pamphyliam petivere intactam idem undique propinqua scirent scirent milite Lycaoniam igitur se igitur cum omnia propinqua documentis Pamphyliam omnia Lycaoniam se per cum parte praesidiis.
                            </p>
                        </Form>
                    </article>
                </Step>
            </div>
        )
    }
}