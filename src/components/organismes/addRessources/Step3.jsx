import   React   from "react"          ;
import { Step  } from "./Wizard";
import   axios   from 'axios'          ;

export default class Step3 extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            categorys: []                            ,
            titre : [],
        };
    }

    componentDidMount() {
        let url = 'https://ressourcesrelationnelles.space//items/';

        axios.get(url + 'rr_categories').then(res => {
                const categorys = res.data.data;
                this.setState({ categorys });
            })
    }

    handleClick = (evt) => {
        document.getElementById("categoryValueCard").textContent = evt.target.value;
    }

    render() {
        return (
            <div className="col-12 content-wizard">
                <Step>
                    <h1 className="title-add-ressource text-center">Ajouter une ressource</h1>
                    <article className="divided-content">
                        <div className="col-6">
                            <h2 className="title-left-content">Catégorie de la ressource</h2>
                            <form>
                                <div className="parent-category-badge-ressource">
                                    {this.state.categorys.map((category, i) =>
                                        <div className="children-category-badge-ressource" key={i}>
                                            <label
                                                htmlFor={"categoryId_" + category.ID_CATEGORIE_RESSOURCE}
                                                className="badge-wizard-data category"
                                            >
                                                {category.DESIGNATION_CATEGORIE_RESSOURCE}
                                            </label>
                                            <input
                                                type="radio"
                                                id={"categoryId_" + category.ID_CATEGORIE_RESSOURCE}
                                                value={category.DESIGNATION_CATEGORIE_RESSOURCE}
                                                name="categoryRessource"
                                                defaultChecked
                                                onClick={this.handleClick}
                                            >
                                            </input>
                                        </div>
                                    )}
                                    <div id="result"></div>
                                </div>
                            </form>
                            <p>
                                La catégorie de la ressource est utilisée comme mot clé pour la fonction de recherche.
                            </p>
                        </div>
                    </article>
                </Step>
            </div>
        )
    }
}