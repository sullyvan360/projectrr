import   React   from "react"          ;
import { Step  } from "./Wizard"
import   axios   from 'axios'          ;

export default class Step4 extends React.Component {
    constructor() {
        super();
        this.state = {
            categorys: []                             ,
        };
    }

    componentDidMount() {
        let url = 'https://ressourcesrelationnelles.space//items/';

        axios.get(url + 'rr_type_relations').then(res => {
                const categorys = res.data.data;
                this.setState({ categorys });
            })
    }

    handleClick = (evt) => {
        document.getElementById("relationsValueCard").textContent = evt.target.value;
    }

    render() {
        return (
            <div className="col-12 content-wizard">
                <Step>
                    <h1 className="title-add-ressource text-center">Ajouter une ressource</h1>
                    <article className="divided-content">
                        <div className="col-6">
                            <h2 className="title-left-content">Type de relation souhaitée</h2>
                                <div className="parent-category-badge-ressource">
                                    {this.state.categorys.map((category, i) =>
                                        <div className="children-category-badge-ressource" key={i}>
                                            <label
                                                htmlFor={"categoryId_" + category.ID_RELATION}
                                                className="badge-wizard-data relation"
                                            >
                                                {category.DESIGNATION_TYPE_RELATION}
                                            </label>
                                            <input
                                                type="radio"
                                                id={"categoryId_" + category.ID_RELATION}
                                                value={category.DESIGNATION_TYPE_RELATION}
                                                name="categoryRessource"
                                                defaultChecked
                                                onClick={this.handleClick}
                                            >
                                            </input>
                                        </div>
                                    )}
                                </div>
                            <p>
                                Le type de relation est utilisée comme mot clé pour la fonction de recherche.
                            </p>
                        </div>
                    </article>
                </Step>
            </div>
        )
    }
}