import   React   from "react"          ;
import { Step  } from "./Wizard"
import   axios   from 'axios'          ;


export default class Step5 extends React.Component {
    constructor() {
        super();
        this.state = {
            categorys: []                             ,
        };
    }

    componentDidMount() {
        let url = 'https://ressourcesrelationnelles.space//items/';

        axios.get(url + 'rr_type_ressources').then(res => {
                const categorys = res.data.data;
                this.setState({ categorys });
            })
    }

    handleClick = (evt) => {
        document.getElementById("ressourceValueCard").textContent = evt.target.value;
    }

    render() {
        return (
            <div className="col-12 content-wizard">
                <Step>
                    <h1 className="title-add-ressource text-center">Ajouter une ressource</h1>
                    <article className="divided-content">
                        <div className="col-6">
                            <h2 className="title-left-content">Type de ressource souhaitée</h2>
                                <div className="parent-category-badge-ressource">
                                    {this.state.categorys.map((category, i) =>
                                        <div className="children-category-badge-ressource" key={i}>
                                            <label
                                                htmlFor={"categoryId_" + category.ID_TYPE_RESSOURCE}
                                                className="badge-wizard-data ressource"
                                            >
                                                {category.DESIGNATION_RESSOURCE}
                                            </label>
                                            <input
                                                type="radio"
                                                id={"categoryId_" + category.ID_TYPE_RESSOURCE}
                                                value={category.DESIGNATION_RESSOURCE}
                                                name="categoryRessource"
                                                defaultChecked
                                                onClick={this.handleClick}
                                            >
                                            </input>
                                        </div>
                                    )}
                                </div>
                            <p>
                                Le type de ressource est utilisée comme mot clé pour la fonction de recherche.
                            </p>
                        </div>
                    </article>
                </Step>
            </div>
        )
    }
}