import React from "react";
import { Step } from "./Wizard"

export default class Step6 extends React.Component {
    constructor() {
        super();

        this.state = {
            date: new Date().toLocaleDateString()
        };
    }

    imageHandleClick = (evt) => {
        var valeur = prompt("Message à afficher");
        let img = document.getElementById("imageURL");

        img.src = valeur;
        document.getElementById("imageURLCard").setAttribute("src",valeur);
    }

    render() {
        return (
            <div id="step6" className="col-12 content-wizard">
                <Step>
                    <h1 className="title-add-ressource text-center">Ajouter une ressource</h1>

                    <article className="divided-content">
                        <div className="col-6">
                            <h2 className="title-left-content">Image de couverture</h2>
                            <div className="add-image-ressources">
                                <div className="add-image-ressources-icons" onClick={this.imageHandleClick}>
                                    <i className="far fa-image"></i>
                                    <i className="add-image-ressources-icons-plus fas fa-plus"></i>
                                    <img id="imageURL" className="add-image-ressources-url" alt="" width="300px" height="300px"/>
                                </div>
                            </div>
                            <p>
                                Le titre de la ressource est utilisé comme mot clé pour la fonction de recherche.
                            </p>

                        </div>
                    </article>
                </Step>
            </div>
        )
    }
}