import   React   from "react"   ;
import { Step  } from "./Wizard"

export default class Step7 extends React.Component {

    onKeyupInput = (evt) => {
        console.log(document.getElementById("contentTextArea").textContent = evt.target.value);
    }

    render() {
        return (
            <div className="col-12 content-wizard">
                <Step>
                    <h1 className="title-add-ressource text-center">Ajouter une ressource</h1>
                    <article className="divided-content">
                        <div className="col-8">
                            <h2 className="title-left-content">Le contenu de la ressource</h2>
                            <p>
                                Le contenu constitura le sujet de la ressource, parlez de tout!
                                Et de ce qu'il vous plait !
                            </p>
                            <form>
                                <textarea
                                    className="notes"
                                    rows="10"
                                    placeholder="Ecrire votre texte ..."
                                    onKeyUp={this.onKeyupInput}
                                >
                                </textarea>
                            </form>
                        </div>
                    </article>
                </Step>
            </div>
        )
    }
}