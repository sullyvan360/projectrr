import React from "react";
import { Step } from "./Wizard"
import axios from 'axios';

import LogoRR from '../../atoms/img/logo/logoResponsive.png';

export default class Step8 extends React.Component {
    constructor(){
        super();

        this.state = {
            utc: new Date().toJSON().slice(0, 10),
            title: document.getElementById("cardTitleRessource").innerText,
            categoryValueCard: document.getElementById("categoryValueCard").innerText,
            relationsValueCard: document.getElementById("relationsValueCard").innerText,
            ressourceValueCard: document.getElementById("ressourceValueCard").innerText,
            contentRessource: document.getElementById("contentTextArea").innerText,
            imageURLCard: document.getElementById("imageURLCard").getAttribute("src"),
        }
    }

    handleSubmitDatas = async () => {
        let url = "https://ressourcesrelationnelles.space//items/";

        const datasToSubmit = {
            DATE_PUBLICATION_RESSOURCE: this.state.utc,
            TITRE_RESSOURCE: this.state.title,
            DESIGNATION_CATEGORIE_RESSOURCE: this.state.categoryValueCard,
            DESIGNATION_TYPE_RELATION: this.state.relationsValueCard,
            DESIGNATION_TYPE_RESSOURCE: this.state.ressourceValueCard,
            CONTENU_RESSOURCE: this.state.contentRessource,
            IMAGE_RESSOURCE: this.state.imageURLCard,
        }

        try {
            if (datasToSubmit.TITRE_RESSOURCE === "") {
                alert("Veuillez nommer votre ressource");
                return false;
            }

            await axios.post(url + 'rr_ressources', datasToSubmit).then(res => {
                if(res.status === 200)
                    window.location = "/ressources_relationnelles/accueil"
            })

        } catch (err) {
            console.error(err)
        }
    }

    render() {
        return (
            <div className="col-12 content-wizard">
                <Step>
                    <h1 className="title-add-ressource text-center">Souhaitez-vous créer cette ressource ?</h1>
                    <article className="parent-last-step">
                        <img src={LogoRR} className="img-last-step" alt="logo Ressources Relationnelles" />
                        <div className="parent-button-last-step">
                            <button onClick={this.handleSubmitDatas} className="button-last-step">Valider ma ressource</button>
                        </div>
                    </article>
                </Step>
            </div>
        )
    }
}