//----- MODULES -----//
import React, { Component } from 'react';

//----- COMPOSANTS -----//
import CommentsContent from '../../molecules/comments/CommentsContent.jsx';

//----- STYLES -----//
import './CommentsContainer.css';

class CommentsContainer extends Component {
    render() {
        return (
            <div className="perimetreCommentsContainer">
                <h2 className="comments-title" id="comments">
                    <span>Commentaires</span>
                </h2>
                <CommentsContent />
            </div>
        )
    }
}
export default CommentsContainer;