import React, { Component } from 'react';
import { NavLink } from "react-router-dom";
import {Button} from 'react-bootstrap';
import ButtonLink from "../../atoms/links/ButtonLink.jsx";

//Images
import LogoMs_Lab from '../../atoms/img/logo/logoMs_Lab.png';
import Logo_Ministere from '../../atoms/img/logo/logoMinistere.png';
import Logo from '../../atoms/img/logo/logoClair.png';

//Icones
import { AiOutlineInstagram, AiFillTwitterCircle } from "react-icons/ai";
import { IoLogoFacebook } from "react-icons/io";

//CSS
import './Footer.css'

const Footer = (props) => {

    const URLcourante = document.location.href.split('/');
    const pageActuelle = URLcourante[4];

    // On vérifie si le route existe ou non
    const activeRoute = (routeName) => {
        return props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
    }

    return (
        <footer className={pageActuelle === "presentation" ? "sectionFooterAjust" : "sectionFooter"}>
            <div className="sectionFooter-first-part">
                <div className="logos-footer">
                    <img src={Logo_Ministere} alt="Logo du ministère" />
                    <img src={LogoMs_Lab} alt="logo Ms_Lab" />
                </div>
                <div className="contact-footer">
                    <ButtonLink class="button-contact-footer" name="nous contacter" url="/ressources_relationnelles/contact"/>
                </div>
                <div className="social-icons-footer">
                    <AiOutlineInstagram />
                    <IoLogoFacebook />
                    <AiFillTwitterCircle />
                </div>
            </div>
            <div className="sectionFooter-second-part">
                <ul className="position-link-footer">
                    {props.routesFooter.map((prop, key) => {
                        if (prop.redirect) return null;
                        return (
                            <li
                                className={
                                    activeRoute(prop.layoutFooter + prop.pathFooter) +
                                    (prop.pro ? "active active-pro" : "")
                                }
                                key={key}
                            >
                                <NavLink
                                    to={prop.layoutFooter + prop.pathFooter}
                                    className="nav-link"
                                    activeclass="active"
                                >
                                    <label>{prop.nameFooter}</label>
                                </NavLink>
                            </li>
                        );
                    })
                    }
                </ul>
            </div>
            <div className="sectionFooter-third-part">
                <img src={Logo} className="logo-ressource-relationnelle-footer" alt="Logo Ressources Relationnelles" />
                <p className="textFooter">&copy; 2021 Ressources Relationnelles Tout droit réservé</p>
            </div>
        </footer>
    );
}
export default Footer;