//*--- MODULES
import React, { Component } from 'react';
import { NavLink, Link } from "react-router-dom";

//*--- COMPOSANTS

//*--- STYLES
import './LeftNavBar.css';
import { GrChapterAdd } from 'react-icons/gr';

export default class LeftNavBar extends Component{
    constructor(props) {
        super(props);
        this.activeRoute.bind(this);
        this.state = {
            ressources : []
        }
        console.log('leftNavBar', props.pro);
      }

    activeRoute(routeName) {
        return this.props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
    }

    render(){
        const URLcourante = document.location.href;
        const splitURL =  URLcourante.split('/');
        const pageActuelle = splitURL[4];

        return(
            <nav className={pageActuelle == "presentation" ? "None" : "LeftNavBar"}>
                <Link className="link-button-wizard-add-ressource" to="/ressources_relationnelles/masterForm">
                    <div className="button-wizard-add-ressource">
                        <i><GrChapterAdd/></i>
                        <span>Ajouter une ressource</span>
                    </div>
                </Link>
                <ul>
                    {this.props.routes.map((prop, key) => {
                        if (prop.redirect) return null;
                            return (
                                <li
                                    className={
                                        this.activeRoute(prop.layout + prop.path) +
                                        (prop.pro ? "active active-pro" : "nav-li-elements")
                                    }
                                    key={key}
                                >
                                    {prop.textSeparation}
                                    <NavLink
                                        to={prop.layout + prop.path}
                                        className="nav-link"
                                        activeclass="active"
                                    >
                                        <i className={prop.icon}></i>
                                        <span className="text-position-nav-bar">{prop.name}</span>
                                    </NavLink>
                                </li>
                            );
                        })
                    }
                </ul>
            </nav>
        );
    }
}