//MODULES
import React, { Fragment, Component } from 'react';
import { NavLink } from "react-router-dom";

//STYLES
import './leftNavBarMyAccount.css';

class LeftNavBarMyAccount extends Component {
    constructor(props) {
        super(props);
        this.activeRoute.bind(this);
        this.state = {

        }
        console.log('leftNavBarAccount', props);
    }
    activeRoute = (routeName) => {
        return this.props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
    }

    render(){
        return(
            <Fragment>
                <nav className="leftNavBarMyAccount">
                    <ul>
                        {this.props.routesNavMyAccount.map((prop, key) => {
                            if (prop.redirect) return null;
                                return (
                                    <li
                                        className={
                                            this.activeRoute(prop.layoutNavMyAccount + prop.pathNavMyAccount) +
                                            (prop.pro ? "active active-pro" : "nav-li-elements")
                                        }
                                        key={key}
                                    >
                                        <NavLink
                                            to={prop.layoutNavMyAccount + prop.pathNavMyAccount}
                                            className="nav-link"
                                            activeclass="active"
                                        >
                                            <img src={prop.iconNavMyAccount}/>
                                            <span>{prop.nameNavMyAccount}</span>
                                        </NavLink>
                                    </li>
                                );
                            })
                        }
                    </ul>
                </nav>
            </Fragment>
        )
    }
}
export default LeftNavBarMyAccount;