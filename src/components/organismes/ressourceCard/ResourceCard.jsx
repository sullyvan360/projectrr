//*----- MODULES -----//
import React, { Component } from 'react';
import { Card }             from 'react-bootstrap';
import axios                from 'axios';

//icons
import heartIcon            from '../../atoms/icons/heartIcon.png';
import commentIcon          from '../../atoms/icons/commentIcon.png';
import shareIcon            from '../../atoms/icons/shareIcon.png';
import starIcon             from '../../atoms/icons/starIcon.png';

//*----- STYLES -----//
import './ResourceCard.css';

class ResourceCard extends Component {
    constructor(props){
        super(props);
        this.state = {
            //*----- STATES BdD -----//
            //------ MEMBRES
            membres                   : props.tableMembres,
            membre                    : props.membre,
            profils                   : [],
            imagesRessources          : [],

            //----- STATES LOCALES -----//
            nombreCommentaireRessource: 0,
            nombrePartageRessource    : 0,
            nombreLikeRessource       : 0,
            visible                   : true,
        };
    }

    componentDidMount(){
        let urlDirectus = 'https://ressourcesrelationnelles.space//items/';

        axios({
            method: 'get',
            url: urlDirectus + 'rr_ressources',
            responseType: 'json',
        }).then(response => {
            this.setState({ profils: [...this.state.profils, ...response.data.data] });
        })
        axios({
            method: 'get',
            url: urlDirectus + 'rr_ressources',
            responseType: 'json',
            params: {
                limit: 9999
            }
        }).then(response => {
            this.setState({ imagesRessources: [...this.state.imagesRessources, ...response.data.data] });
        });
    }

    render(){
        return(
            <div id="resourceCard" className="resourceCard">
                <div id="lbl_publicationResourceCard" className="col-12 lbl_publicationResourceCard">
                    <span id="span_publicationResourceCard" className="span_publicationResourceCard">publié le : <strong>{this.props.tableRessources.DATE_PUBLICATION_RESSOURCE}</strong></span>
                </div>

                <div className="cardRessource">
                    <div className="cardBody">
                        <Card.Body>
                            <div className="row">
                                <div className="titleResourceCard col-12">
                                    <Card.Title>
                                        <strong className="tailleTitreCardRessources">{this.props.tableRessources.TITRE_RESSOURCE}</strong>
                                    </Card.Title>
                                </div>
                            </div>
                            <div className="row">
                                <div className="contentPictureResourceCard col-12">
                                    <Card.Img
                                        style={{alignItems: 'center'}}
                                        src={this.props.tableRessources.IMAGE_RESSOURCE} alt=""
                                        width="290px"
                                        height="160px"
                                    />
                                </div>
                            </div>
                            <div className="colorCardBadge">
                                <span className="badgeRessourceCard" variant="primary">{this.props.tableRessources.DESIGNATION_CATEGORIE_RESSOURCE}</span>
                                <span className="badgeRessourceCard" variant="secondary">{this.props.tableRessources.DESIGNATION_TYPE_RESSOURCE}</span>
                                <span className="badgeRessourceCard" variant="secondary">{this.props.tableRessources.DESIGNATION_TYPE_RELATION}</span>
                            </div>
                            <div className="row">
                                <div className="ProfilPictureResourceCard col-12">
                                    <Card.Img
                                        style={{width: '40px'}}
                                        width="40px"
                                        src={this.props.tableRessources.IMAGE_PROFIL_MEMBRE}
                                    />
                                    <span className="span_PseudonymeMembreResourceCard"><strong>{this.props.tableRessources.PSEUDO_MEMBRE}</strong></span>
                                    <span className="span_ProfessionMembreResourceCard">{this.props.tableRessources.PROFESSION_MEMBRE}</span>
                                </div>
                            </div>
                        </Card.Body>
                    </div>
                    <Card.Footer className="FooterResourceCard">
                        <div className="row">
                            <div className="iconResourceCard col-12">
                                <span><IconStatResourceCard name={heartIcon} number={this.props.tableRessources.NOMBRE_LIKE}/></span>
                                <span><IconStatResourceCard name={commentIcon} number={this.props.tableRessources.NOMBRE_COMMENTAIRE}/></span>
                                <span><IconStatResourceCard name={shareIcon} number={this.props.tableRessources.NOMBRE_PARTAGE}/></span>
                                <span><IconStatResourceCard name={starIcon}/></span>
                            </div>
                        </div>
                    </Card.Footer>
                </div>
            </div>
        );
    }
}

function IconStatResourceCard(props){
    return(
        <div>
            <Card.Img src={props.name}/>
            <div className="ligneStat">
                <span className="span_numberIconResourceCard">{props.number}</span>
            </div>
        </div>
    )
}

export default ResourceCard;