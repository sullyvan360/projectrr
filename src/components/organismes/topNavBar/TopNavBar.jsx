//----- MODULES
import React, { Component } from 'react';

//----- COMPOSANTS
import LogInButton   from "../../molecules/buttons/logInButton/LogInButton";
import SignInButton  from "../../molecules/buttons/signInButton/SignInButton";
import ButtonLink from "../../atoms/links/ButtonLink.jsx";
import GuestAvatar   from "../../atoms/img/monCompte/guest.png";

//----- STYLES
import './TopNavBar.css';

//----- MULTIMEDIA
import Logo        from '../../atoms/img/logo/logoSombre.png';
import iconSolaire from '../../atoms/icons/iconSolaire.png';

export default class TopNavBar extends Component {
    constructor(props){
        super(props);
        this.state={
            user: "Guest"
        }
    };
    render()
    {
        return(
            <div className="backgroundTopNavBar">
                <img src={Logo} className="imgTopNavBar" alt="ressourcesRelationnelles" />
                <div className="positionButtonConnexionInscription">
                    <ButtonLink class="leftNavBar-bouton" name="Présentation" url="/ressources_relationnelles/presentation"/>
                    <ButtonLink class="leftNavBar-bouton" name="Contact" url="/ressources_relationnelles/contact"/>
                    <ButtonLink class="leftNavBar-bouton" name="Aide" url ="/ressources_relationnelles/help"/>
                    <LogInButton text="Se Connecter" />
                    <SignInButton />
                    <img src={iconSolaire} className="iconSolaireTopNavBar" alt="ressourcesRelationnelles" />
                    <div className="sessionActuelle">
                        <img src={GuestAvatar} alt="" />
                        <span>{this.state.user}</span>
                    </div>
                </div>
            </div>
        );
    }
}