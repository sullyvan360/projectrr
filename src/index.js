import React from 'react';
import ReactDOM from 'react-dom';
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";

import Layouts from "./layouts/Layouts.jsx";

import './index.css';
import '../node_modules/bootstrap/dist/css/bootstrap.css';

const hist = createBrowserHistory();

ReactDOM.render(
  <Router history={hist}>
    <Switch>
      <Route path="/ressources_relationnelles" render={(props) => <Layouts {...props} />} />
      <Redirect to="/ressources_relationnelles/presentation" />
    </Switch>
  </Router>,
  document.getElementById('root'),
);