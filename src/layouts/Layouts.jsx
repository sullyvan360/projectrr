import React from "react";

import { Route, Switch } from "react-router-dom";

// le core des components
import LeftNavBar from "../components/organismes/leftNavBar/LeftNavBar.jsx";
import TopNavBar from "../components/organismes/topNavBar/TopNavBar.jsx";
import Footer from "../components/organismes/footer/Footer.jsx"

import routesTop from "../routes/routesTop.jsx";
import routes from "../routes/routes.jsx";
import routesFooter from "../routes/routesFooter.jsx";


export default class Layouts extends React.Component {

  render() {
    return (
      <main>
        <TopNavBar
          {...this.props} routesTop={routesTop} />

        <LeftNavBar
          {...this.props} routes={routes} />

          <Switch>
            {routesTop.map((prop, key) => {
              return (
                <Route
                  path={prop.layoutTop + prop.pathTop}
                  component={prop.component}
                  key={key}
                />
              );
            })}
          </Switch>

          <Switch>
            {routes.map((prop, key) => {
              return (
                <Route
                  path={prop.layout + prop.path}
                  component={prop.component}
                  key={key}
                />
              );
            })}
          </Switch>

          <Switch>
            {routesFooter.map((prop, key) => {
              return (
                <Route
                  path={prop.layoutFooter + prop.pathFooter}
                  component={prop.component}
                  key={key}
                />
              );
            })}
          </Switch>

        <Footer {...this.props} routesFooter={routesFooter}/>
      </main>
    );
  }
}
