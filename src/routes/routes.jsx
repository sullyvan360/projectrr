//Import pour les vues
import Home from "../views/home/Home.jsx";
import Trends from "../views/trends/Trends.jsx";
import Forum from "../views/forum/Forum.jsx";
import Self from "../views/self/Self.jsx";
import Love from "../views/love/Love.jsx";
import Professional from "../views/professional/Professional.jsx";
import Family from "../views/family/Family.jsx";
import MasterForm from "../views/addRessources/ViewAddRessources.jsx";

import Friends from "../views/friends/Friends.jsx";
import MonCompte from "../views/monCompte/MonCompte.jsx";


const dashRoutes = [
  {
    textSeparation:<p className="positionTextSeparationLeftNavBar">Explorer</p>,
    path: "/accueil",
    name: "Accueil",
    icon: "fas fa-home",
    component: Home,
    layout: "/ressources_relationnelles",
  },
  {
    path: "/trends",
    name: "Tendances",
    icon: "fas fa-fire",
    component: Trends,
    layout: "/ressources_relationnelles",
  },
  {
    path: "/moncompte",
    name: "Mon Compte",
    icon: "fas fa-user",
    component: MonCompte,
    layout: "/ressources_relationnelles",
  },
  {
    path: "/forum",
    name: "Forum",
    icon: "fas fa-comment-alt",
    component: Forum,
    layout: "/ressources_relationnelles",
  },
  {
    textSeparation:<p className="positionTextSeparationLeftNavBar">Améliorer vos relations</p>,
    path: "/self",
    name: "Soi",
    icon: "fas fa-child",
    component: Self,
    layout: "/ressources_relationnelles",
  },
  {
    path: "/love",
    name: "Amour",
    icon: "fas fa-hand-holding-heart",
    component: Love,
    layout: "/ressources_relationnelles",
  },
  {
    path: "/professional",
    name: "Professionnel",
    icon: "fas fa-briefcase",
    component: Professional,
    layout: "/ressources_relationnelles",
  },
  {
    path: "/family",
    name: "Familles",
    icon: "fas fa-people-arrows",
    component: Family,
    layout: "/ressources_relationnelles",
  },
  {
    path: "/friends",
    name: "Amis",
    icon: "fas fa-people-carry",
    component: Friends,
    layout: "/ressources_relationnelles",
  },
  {
    path: "/masterForm",
    icon: "fas fa-home",
    component: MasterForm,
    layout: "/ressources_relationnelles",
  },
];

export default dashRoutes;
