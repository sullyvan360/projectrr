//Import pour les vues
import Help from '../views/help/Help.jsx';
import Faq from '../views/help/faq/FAQ.jsx';
import About from '../views/help/about/About.jsx';
import Conditions from '../views/help/conditions/Conditions.jsx';
import PoliticalCookies from '../views/help/politicalCookies/PoliticalCookies.jsx';
import Conduite from '../views/help/conduite/Conduite.jsx';

const footerRoutes = [
  {
    pathFooter: "/help",
    nameFooter: "Aides",
    component: Help,
    layoutFooter: "/ressources_relationnelles",
  },
  {
    pathFooter: "/faq",
    nameFooter: "FAQ",
    component: Faq,
    layoutFooter: "/ressources_relationnelles",
  },
  {
    pathFooter: "/about",
    nameFooter: "A propos",
    component: About,
    layoutFooter: "/ressources_relationnelles",
  },
  {
    pathFooter: "/conditions",
    nameFooter: "Conditions",
    component: Conditions,
    layoutFooter: "/ressources_relationnelles",
  },
  {
    pathFooter: "/politicalCookies",
    nameFooter: "Politique de Cookies",
    component: PoliticalCookies,
    layoutFooter: "/ressources_relationnelles",
  },
  {
    pathFooter: "/conduite",
    nameFooter: "Lignes de Conduite",
    component: Conduite,
    layoutFooter: "/ressources_relationnelles",
  }
];
export default footerRoutes ;
