//VIEWS
import Preview    from '../views/monCompte/preview/Preview';
import Gallery    from '../views/monCompte/gallery/Gallery';
import Favorites  from '../views/monCompte/favorites/Favorites';
import Resources  from '../views/monCompte/resources/Resources';
import About      from '../views/monCompte/about/About';

//ICONS
import PreviewIcon    from '../components/atoms/img/monCompte/preview.png';
import GalleryIcon    from '../components/atoms/img/monCompte/gallery.png';
import FavoritesIcon  from '../components/atoms/img/monCompte/favorite.png';
import ResourcesIcon  from '../components/atoms/img/monCompte/resources.png';
import AboutIcon      from '../components/atoms/img/monCompte/about.png';

const routesNavMyAccount = [
  {
    pathNavMyAccount  : "/aperçu",
    nameNavMyAccount  : "Aperçu",
    iconNavMyAccount  : PreviewIcon,
    component         : Preview,
    layoutNavMyAccount: "/ressources_relationnelles/moncompte",
  },
  {
    pathNavMyAccount  : "/galerie",
    nameNavMyAccount  : "Galerie",
    iconNavMyAccount  : GalleryIcon,
    component         : Gallery,
    layoutNavMyAccount: "/ressources_relationnelles/moncompte",
  },
  {
    pathNavMyAccount  : "/favoris",
    nameNavMyAccount  : "Favoris",
    iconNavMyAccount  : FavoritesIcon,
    component         : Favorites,
    layoutNavMyAccount: "/ressources_relationnelles/moncompte",
  },
  {
    pathNavMyAccount  : "/ressources",
    nameNavMyAccount  : "Ressources",
    iconNavMyAccount  : ResourcesIcon,
    component         : Resources,
    layoutNavMyAccount: "/ressources_relationnelles/moncompte",
  },
  {
    pathNavMyAccount  : "/apropos",
    nameNavMyAccount  : "A propos",
    iconNavMyAccount  : AboutIcon,
    component         : About,
    layoutNavMyAccount: "/ressources_relationnelles/moncompte",
  }
];
export default routesNavMyAccount;