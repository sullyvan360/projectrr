//Import pour les vues
import Presentation from '../views/presentation/Presentation.jsx';
import Contact from '../views/contact/Contact.jsx';

const topRoutes = [
    {
        pathTop: "/presentation",
        nameTop: "Présentation",
        component: Presentation,
        layoutTop: "/ressources_relationnelles",
    },
    {
        pathTop: "/contact",
        nameTop: "Contact",
        component: Contact,
        layoutTop: "/ressources_relationnelles",
    },
];
export default topRoutes;