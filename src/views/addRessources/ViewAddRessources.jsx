import React from 'react';

//COMPONENTS --> Organismes
import { Wizard } from "../../components/organismes/addRessources/Wizard"
import Step1 from "../../components/organismes/addRessources/Step1"
import Step2 from "../../components/organismes/addRessources/Step2"
import Step3 from "../../components/organismes/addRessources/Step3"
import Step4 from "../../components/organismes/addRessources/Step4"
import Step5 from "../../components/organismes/addRessources/Step5"
import Step6 from "../../components/organismes/addRessources/Step6"
import Step7 from "../../components/organismes/addRessources/Step7"
import Step8 from "../../components/organismes/addRessources/Step8"

//CSS
import './ViewAddRessources.css'
import '../../components/organismes/addRessources/Step.css'
import '../../components/organismes/addRessources/Wizard.css'

export default class ViewAddRessources extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      step : 0,
    }
  }
  // state = { step: 0 };

  handleStep = step => {
    this.setState({ step });
  };


  render() {
    return (
      <div className="container margin-top55">
        <div className="row">
          <div className="parent-add-ressources">
            <Wizard step={this.state.step} onChange={this.handleStep}>
              <Step1 step={this.state.step} title="Introduction" />
              <Step2 step={this.state.step} title="Titre" />
              <Step3 step={this.state.step} title="Catégorie" />
              <Step4 step={this.state.step} title="Type de Relation" />
              <Step5 step={this.state.step} title="Type de Ressource " />
              <Step6 step={this.state.step} title="Image de couverture" />
              <Step7 step={this.state.step} title="Contenu" />
              <Step8 step={this.state.step} title="Finalisation" />
            </Wizard>
          </div>
        </div>
      </div>
    );
  }
}