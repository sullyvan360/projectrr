import React, { Component } from 'react';

//CSS
import './Contact.css';

//Logo
import Logo from '../../components/atoms/img/logo/logoSombre.png';

export default class Contact extends Component {
	componentDidMount() {
		window.scrollTo(0, 0);
	}

	render() {
		return (
			<section id="ContactForm">
				<h1 className="titleVisitorSitePresentation">Contacter la communauté</h1>
				<div className="container margin-top100">
					<div className="row">
						<ul className="contact-list">
							<li className="col-3">
								<i className="fas fa-map-marker-alt"></i>
								<span><strong>Adresse: </strong>10 rue des Mirabelles 18000 Bourges</span>
							</li>
							<li className="col-3">
								<i className="fas fa-phone-alt"></i>
								<span><a href="tel:+0225425621">02 25 42 56 21</a></span>
							</li>
							<li className="col-3">
								<i className="far fa-paper-plane"></i>
								<span><a href="mailto:ms-lab@outlook.com">ms-lab@outlook.com</a></span>
							</li>
							<li className="col-3">
								<i className="fas fa-globe-europe"></i>
								<span><a href="http://ms-lab.fr">ms-lab.fr</a></span>
							</li>
						</ul>
					</div>
					<div className="row">
						<div className="col-8 contact-form-data">
							<div className="form-group">
								<label>Nom</label>
								<input type="text" className="form-control" id="nomContact" name="nomContact" placeholder="Nom" required/>
							</div>
							<div className="form-group">
								<label>Prénom</label>
								<input type="text" className="form-control" id="prenomContact" name="prenomContact" placeholder="Prénom" required/>
							</div>
							<div className="form-group">
								<label>Objet</label>
								<input type="text" className="form-control" id="subject" name="subject" placeholder="Objet" required/>
							</div>
							<div className="form-group">
								<label>Message</label>
								<textarea className="form-control" type="textarea" id="message" placeholder="Message" maxLength="140" rows="7"></textarea>
							</div>
							<button type="button" id="submit" name="submit" className="btn btn-primary pull-right">Envoyer</button>
						</div>
						<div className="col-4 contact-form-image">
							<img src={Logo} alt="Logo Ressources Relationnelles" />
						</div>
					</div>
				</div>

			</section>
		);
	}
}