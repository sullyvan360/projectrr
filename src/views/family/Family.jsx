//*----- MODULES -----//
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

//*----- COMPOSANTS -----//
import ResourceCard from '../../components/organismes/ressourceCard/ResourceCard.jsx';
import Spinner from '../../components/molecules/spinner/spinner'

//*----- STYLES -----//
import './Family.css';

export default class Family extends Component {
  _isMounted = false;

  //*----- INSTANCIATION -----//
  constructor(props) {
    super(props);
    this.state = {
      ressources: [],
      membres: [],
      url: `https://ressourcesrelationnelles.space//items/rr_ressources?filter[DESIGNATION_TYPE_RELATION][_eq]=Famille`,

      loading: true,
    }
  }

  componentDidMount() {
    this._isMounted = true;

    axios.get(this.state.url).then(response => {
      if (this._isMounted) {
        this.setState({ ressources: [...this.state.ressources, ...response.data.data] });
        this.setState({ loading: false });
      }
    })

    window.scrollTo(0, 0);
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const loadingSpinner = this.state.loading;

    return (
      <section>
       {loadingSpinner ? <Spinner classSpinner="loading-spinner-relative"/> :
          <div className="parent-display-ressources">
            <ul className="display-ressources-list">
              {this.state.ressources.map((ressource, indexRessource) => {
                return (
                  <Link key={indexRessource} to="/ressources_relationnelles/resourcecontainer">
                    <ResourceCard tableRessources={ressource} tableMembres={this.state.membres} />
                  </Link>
                )
              })}
            </ul>
          </div>
        }
      </section>
    )
  }
}