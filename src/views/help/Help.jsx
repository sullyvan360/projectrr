//----- MODULES
import React, { Component } from 'react';
import { CardGroup, Card } from 'react-bootstrap'
import { Link } from 'react-router-dom';

//----- STYLES
import './Help.css';

//----- MULTIMEDIAS
import aboutUsHelp  from '../../components/atoms/img/help/aboutUs.png';
import conditionsHelp from '../../components/atoms/img/help/conditions.png';
import conduitesHelp from '../../components/atoms/img/help/conduites.png';
import cookiesHelp from '../../components/atoms/img/help/Cookies.png';
import faqHelp from '../../components/atoms/img/help/FAQ.png';


export default class Help extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render(){
    return(
      <section id="remonterHaut">
        <h1 className="titleVisitorSitePresentation">Aide des services de média sociaux</h1>
        <CardGroup>
        <div className="positionCardParentHelp col-12">
          <div className="positionCardHelp">
          <Link to="/ressources_relationnelles/faq">
            <Card.Img variant="top" src={faqHelp} className="imgCardposition" />
            <Card.Body className="contenuBodyCardHelp">
              <Card.Title className="titreCardHelp">FAQ</Card.Title>
              <Card.Text>
                This is a wider card with supporting text below as a natural lead-in to
                additional content. This content is a little bit longer.
              </Card.Text>
            </Card.Body>
          </Link>
          </div>
          <div className="positionCardHelp">
            <Link to="/ressources_relationnelles/politicalCookies">
              <Card.Img variant="top" src={cookiesHelp} className="imgCardposition"/>
              <Card.Body className="contenuBodyCardHelp">
                <Card.Title className="titreCardHelp">Politique de Cookies</Card.Title>
                <Card.Text>
                  This is a wider card with supporting text below as a natural lead-in to
                  additional content. This content is a little bit longer.
                </Card.Text>
              </Card.Body>
            </Link>
          </div>
          <div className="positionCardHelp">
            <Link to="/ressources_relationnelles/conduite">
              <Card.Img variant="top" src={conduitesHelp} className="imgCardposition"/>
              <Card.Body className="contenuBodyCardHelp">
                <Card.Title className="titreCardHelp">Ligne de Conduites</Card.Title>
                <Card.Text>
                  This is a wider card with supporting text below as a natural lead-in to
                  additional content. This content is a little bit longer.
                </Card.Text>
              </Card.Body>
            </Link>
          </div>
        </div>

        <div className="positionCardParentHelp col-12">
          <div className="positionCardHelp">
            <Link to="/ressources_relationnelles/about">
              <Card.Img variant="top" src={aboutUsHelp} className="imgCardposition"/>
              <Card.Body className="contenuBodyCardHelp">
                <Card.Title className="titreCardHelp">A propos</Card.Title>
                <Card.Text>
                  This is a wider card with supporting text below as a natural lead-in to
                  additional content. This content is a little bit longer.
                </Card.Text>
              </Card.Body>
            </Link>
          </div>
          <div className="positionCardHelp">
            <Link to="/ressources_relationnelles/conditions">
              <Card.Img variant="top" src={conditionsHelp} className="imgCardpositionConditions"/>
              <Card.Body className="contenuBodyCardHelp">
                <Card.Title className="titreCardHelp">Conditions</Card.Title>
                <Card.Text>
                  This is a wider card with supporting text below as a natural lead-in to
                  additional content. This content is a little bit longer.
                </Card.Text>
              </Card.Body>
              </Link>
          </div>
        </div>
        </CardGroup>
      </section>
    );
  }
}