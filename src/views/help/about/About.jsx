import React, {Component} from 'react';

//CSS
import './About.css';

//Logo
import Logo from '../../../components/atoms/img/logo/logoSombre.png';

export default class About extends Component {
	componentDidMount() {
		window.scrollTo(0, 0);
	  }

	render()
	{
		return(
			<section>
				<h1 className="titleVisitorSitePresentation">A Propos de la communauté</h1>

				<div className="container parent-position-element-conditions parent-about">
					<div className="row">
						<div className="child-about-informations">
							<div className="col-12 logo">
								<img src={Logo} alt="Logo Ressources Relationnelles"/>
							</div>
							<div className="col-6 text">
								<p>Notre mission est que chacun puisse faire entendre sa voix et découvrir le monde.</p>
								<p>Nous pensons que chacun mérite de faire entendre sa voix, et que le monde n'en est que meilleur lorsque nous écoutons les autres, partageons nos expériences et développons des communautés grâce à nos histoires.</p>
								<p>Nos valeurs se basent sur quatre libertés fondamentales qui définissent ce que nous sommes.</p>
							</div>
						</div>
					</div>

					<div className="row">
						<div className="child-about-liberty">
							<ul className="col-11">
								<li>
									<span>Liberté <strong className="background-under">d'expression</strong></span>
									<p>Nous pensons que chacun devrait pouvoir s'exprimer librement, partager ses opinions et encourager le dialogue, et que cette liberté créative conduit à l'émergence de points de vue, de possibilités et de formats innovants.</p>
								</li>
								<li>
									<span>Liberté <strong className="background-under">d'information</strong></span>
									<p>Nous pensons que chacun devrait pouvoir accéder facilement et sans limites à l'information. Nous sommes convaincus que la vidéo est un outil puissant pour enseigner, favoriser la compréhension et traiter des événements mondiaux, qu'ils soient majeurs ou non.</p>
								</li>
								<li>
									<span>Liberté <strong className="background-under">d'opportunité</strong></span>
									<p>Nous pensons que chacun devrait avoir l'occasion d'être découvert, de créer son activité et de réussir en faisant ses propres choix. Nous sommes convaincus que tout le monde, et non une minorité de personnes de pouvoir, devrait pouvoir décider de ce qui est populaire.</p>
								</li>
								<li>
									<span>Liberté <strong className="background-under">d'appartenance</strong></span>
									<p>Nous pensons que tout le monde devrait pouvoir trouver des communautés d'entraide, faire tomber les barrières, dépasser les frontières et se retrouver autour de passions et de centres d'intérêt communs.</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
		);
	}
}