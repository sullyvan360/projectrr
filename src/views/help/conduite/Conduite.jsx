import React, {Component} from 'react';

//CSS

export default class Conduites extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render(){
    return(
      <section>
          <h1 className="titleVisitorSitePresentation">Lignes de Conduites de la communauté</h1>

          <div className="col-12 parent-position-element-conditions">
                <ol className="col-10">
                  <div>
                    <li>Infraction à la loi</li>
                    <p>
                      Lorsque vous utilisez nos services, vous devez respecter toutes les lois locales, nationales et internationales applicables. Tout contenu ou activité mettant en avant, encourageant, proposant ou sollicitant une activité illégale est interdite.
                      Cela inclut la perpétuation ou l’aide à la destruction malveillante, à la dégradation ou au vol de biens publics ou privés d’une autre personne sans autorisation pendant le stream.
                    </p>
                  </div>
                  <div>
                    <li>Contournement d’une suspension</li>
                    <p>
                      Toutes les suspensions doivent être respectées jusqu’à leur expiration ou leur annulation suite à une procédure d’appel réussie. Toute tentative pour contourner une suspension de compte ou un bannissement du chat grâce à d’autres comptes, identités, ou en apparaissant sur le compte d’un autre utilisateur conduira également à une sanction supplémentaire contre vos comptes, jusqu’à une suspension indéfinie. 
                      De plus, il est interdit d’utiliser votre chaîne pour présenter ou annoncer sciemment un utilisateur suspendu. Nous comprenons que dans certaines situations, les utilisateurs suspendus apparaissent sur votre stream en raison de circonstances qui échappent à votre contrôle, comme lors de tournois de jeux tiers. Nous comptons toutefois sur vous pour agir de bonne foi et les retirer de votre diffusion, les empêcher de communiquer ou tout du moins, limiter leurs interactions avec votre stream.
                    </p>
                  </div>
                  <div>
                    <li>Comportement auto-destructeur</li>
                    <p>
                      Tout comportement susceptible de mettre en danger votre vie ou de porter atteinte à votre intégrité physique est interdit. Cela inclut, sans pour autant s’y limiter, les menaces de suicide, les traumatismes physiques intentionnels, l’usage de substances illicites, la consommation illégale ou excessive d’alcool, et le fait de conduire dangereusement ou en étant distrait. Nous ne faisons aucune exception pour les comportements auto-destructeurs accomplis pour plaisanter ou pour divertir, lorsque le comportement est susceptible de causer des blessures physiques.
                    </p>
                  </div>
                  <div>
                    <li>Violence et menaces</li>
                    <p>
                      Les actes et menaces de violence seront pris au sérieux et sont considérés comme des violations qui seront traitées avec une tolérance zéro. Tous les comptes associés à de telles activités seront suspendus pour une durée indéterminée. Cela inclut, sans pour autant s’y limiter.
                      Ressources Relationnelles n’autorise pas les contenus qui représentent, font l’apologie, encouragent ou soutiennent le terrorisme, ou encore les individus ou actes extrémistes violents. Cela inclut le fait de menacer ou d’encourager d’autres personnes à commettre des actes qui entraîneraient des dommages physiques graves chez des groupes de personnes ou une destruction importante de biens. Il est interdit d’afficher ou de publier des liens de propagande terroriste ou extrémiste, y compris des images ou des séquences graphiques de violence terroriste ou extrémiste, et ce même dans le but de dénoncer un tel contenu.
                      Dans les situations où un utilisateur perd le contrôle de son stream en raison d’une blessure grave, d’une urgence médicale, d’une intervention policière ou d’une violence grave dont il aurait fait l’objet, nous suspendrons temporairement la chaîne et retirerons le contenu associé.
                      Dans des circonstances exceptionnelles, nous pouvons suspendre des comptes de manière anticipée, lorsque nous pensons que l’utilisation de Ressources Relationnelles par un individu a une très grande probabilité d’inciter à la violence. Lorsque nous évaluons les risques potentiels, nous prenons en considération l’influence de l’individu, le niveau de dangerosité de son comportement par le passé (que ce comportement passé se soit produit sur Ressources Relationnelles, ou non), si cet individu constitue toujours un risque, ainsi que la portée des menaces en cours.
                    </p>
                  </div>
                  <div>
                    <li>Comportements haineux et harcèlement</li>
                    <p>
                      Les comportements haineux et le harcèlement ne sont pas tolérés sur Ressources Relationnelles. Les comportement haineux désignent tout contenu ou toute activité qui promeut ou encourage la discrimination, le dénigrement, le harcèlement ou la violence sur la base des caractéristiques protégées suivantes : race, ethnie, couleur, caste, origine nationale, statut d’immigration, religion, sexe, genre, identité sexuelle, orientation sexuelle, handicap, état de santé grave et statut d’ancien combattant. Nous offrons également certaines protections pour l’âge. Ressources Relationnelles applique une tolérance zéro en ce qui concerne les comportements haineux, ce qui signifie que nous prenons des mesures pour chaque cas de comportement haineux valable signalé. En vertu de cette politique, nous offrons à tous les utilisateurs les mêmes protections, quelles que soient leurs caractéristiques individuelles.
                      Le harcèlement peut se manifester de nombreuses façons, comme le fait de suivre une personne à la trace, les attaques personnelles, l’apologie de violences physiques, les raids hostiles, et les faux signalements à plusieurs. Quant au harcèlement sexuel spécifiquement, il peut prendre la forme d’avances et de demandes sexuelles non souhaitées ou d’attaques dégradantes liées aux pratiques sexuelles perçues.
                      Nous prendrons des mesures pour tous les cas de comportements haineux et de harcèlement, avec une sévérité croissante lorsque le comportement est ciblé, personnel, explicite, répété ou prolongé, qu’il incite à d’autres exactions ou qu’il implique des menaces de violence ou de coercition. Les infractions les plus graves peuvent entraîner une suspension définitive dès le premier incident. 
                      En savoir plus sur nos politiques en matière de harcèlement et de comportements haineux et leur application.
                    </p>
                  </div>
                  <div>
                    <li>Partage non-autorisé d’informations privées</li>
                    <p>
                      Ne portez pas atteinte à la vie privée d’autrui. Il est interdit de partager du contenu pouvant révéler des informations personnelles privées sur des personnes ou leur propriété privée sans autorisation. Voici une liste non exhaustive des cas concernés :
                    </p>
                    <ul>
                        <li>Partage d’informations permettant d’identifier quelqu’un (comme son nom de famille officiel, le lieu où il vit ou ses papiers d'identité)</li>
                        <li>Partage de profils de réseaux sociaux restreints ou privés ou de toute information provenant de ces profils</li>
                        <li>Partage de contenus qui porte atteinte à l’attente raisonnable d’une autre personne en matière de vie privée, comme le fait de streamer depuis un espace privé sans autorisation</li>
                    </ul>
                  </div>
                  <div>
                    <li>Usurpation d’identité</li>
                    <p>
                      Tout contenu ou toute activité visant à usurper l’identité d’une personne ou entité est interdit. Toute tentative visant à vous faire passer pour un membre des représentants de Ressources Relationnelles constitue une violation qui sera traitée avec une tolérance zéro et entraînera une suspension pour une durée indéterminée.
                    </p>
                  </div>
                  <div>
                    <li>Nudité, pornographie et autre contenu à caractère sexuel</li>
                    <p>
                      La nudité et le contenu ou activités sexuellement explicites, tels que la pornographie, les actes ou rapports sexuels, et les services sexuels sont interdits.
                      Le contenu ou activités qui menacent ou incitent à la violence ou à l’exploitation sexuelle sont strictement interdits et peuvent être signalés aux forces de l’ordre. L’exploitation de mineurs sera signalée aux autorités via le National Center for Missing & Exploited Children.
                      Le contenu ou les activités à caractère sexuel sont également interdits, bien qu’ils puissent être autorisés dans des contextes éducatifs ou pour du contenu sous licence approuvé au préalable, dans chaque cas sous réserve de restrictions supplémentaires.
                    </p>
                  </div>
                  <div>
                    <li>Violence extrême, gore et autres comportements obscènes</li>
                    <p>
                      Tout contenu exclusivement centré sur des scènes sanglantes ou de violence gratuite ou extrême est interdit.
                    </p>
                  </div>
                </ol>
          </div>
      </section>
    );
    }
  }