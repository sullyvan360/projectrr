//*----- MODULES -----//
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

//*----- COMPOSANTS -----//
import ResourceCard from '../../components/organismes/ressourceCard/ResourceCard.jsx';
import Spinner from '../../components/molecules/spinner/spinner'

//CSS
import './Home.css';

const Home = (props) => {
  const url = 'http://directus-ressources-relationnelles.fr.ms-lab.eu/items/rr_ressources?limit=20'
  const [ressources, setRessources] = useState([]);
  const [membres, setMembres] = useState('');
  const [page, setPage] = useState(2);
  const [isFetching, setIsFetching] = useState(false);
  const [isLoading, setIsLoading] = useState(true)

  const loadData = () => {
    axios.get(url).then(res => {
      setRessources(res.data.data);
      setIsLoading(false)
    });
    window.scrollTo(0, 0);
  }

  const moreData = () => {
    var datasRessources = url + `&page=${page}`;
    axios.get(datasRessources).then(res => {
      setRessources([...ressources, ...res.data.data]);
      setPage(page + 1)
      setIsFetching(false)
    });
  }

  const isScrolling = () => {
    let bottomPage = window.innerHeight + document.documentElement.scrollTop !== document.documentElement.offsetHeight;
    if (bottomPage) {
      return;
    }
    setIsFetching(true)
  }

  useEffect(() => {
    loadData()
    window.addEventListener("scroll", isScrolling);
    return () => window.removeEventListener("scroll", isScrolling);
  }, [])

  useEffect(() => {
    if (isFetching) {
      document.getElementById("parentLoadingData").classList.add("isFetching")
      moreData();
    }
  }, [isFetching]);

  return (
    <>
      <section>
      {isLoading ? <Spinner classSpinner="loading-spinner-relative" /> :
          <div className="parent-display-ressources">
            <ul className="display-ressources-list">
              {ressources.map((ressource, i) =>
                <Link key={i} to="/ressources_relationnelles/resourcecontainer">
                  <ResourceCard tableRessources={ressource} tableMembres={membres} />
                </Link>
              )}
            </ul>
            {isFetching ? <Spinner classSpinner="loading-spinner-absolute" /> : ""}
          </div>
        }
      </section>
    </>
  );
}

export default Home;