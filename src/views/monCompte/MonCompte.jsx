//MODULES
import React, { Fragment, Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

//COMPONENTS
import LeftNavBarMyAccount from '../../components/organismes/leftNavBarMyAccount/LeftNavBarMyAccount.js';
import routesNavMyAccount from '../../routes/routesNavMyAccount.js';
import ImgProfil from '../../components/atoms/img/ressources/resourceCardAvatar.png';
import backgroundContentMyAccount from '../../components/atoms/img/monCompte/rectangleMonCompte.png';

//STYLES
import './MonCompte.css';

class MonCompte extends Component {
	constructor(props){
		super(props);
		this.state={

		}
	}
	render(){
		return(
			<Fragment>
				<section>
					<div className="componentMyAccount">
						<div className="divLeftNavBarMyAccount">
							<div>
								<div>
									<img src={ImgProfil}/>
									<span>John Doe</span>
									<div>Développeur informatique</div>
								</div>
								<LeftNavBarMyAccount
									{...this.props}
									routesNavMyAccount={routesNavMyAccount}
								/>
							</div>
							<img src={backgroundContentMyAccount} />
							<Switch>
								{routesNavMyAccount.map((prop, key) => {
								return(
									<Route
										path={prop.layoutNavMyAccount + prop.pathNavMyAccount}
										component={prop.component}
										key={key}
									/>
								);
								})}
							</Switch>
						</div>
						<div className="divContentMyAccount">

						</div>
					</div>
				</section>
			</Fragment>
	  	)
  	}
}
export default MonCompte;