//----- MODULES
import React, { Component } from 'react';
import { Link }             from 'react-router-dom';

//----- COMPOSANTS

//----- STYLES
import './Presentation.css';

//----- MULTIMEDIAS
import ImagePresentation from '../../components/atoms/img/home/img_socialMediaHome.png';
import Logo   from '../../components/atoms/img/logo/logoSombre.png';
import Image1 from '../../components/molecules/presentation/templates/template1/images/image1.png';
import Image2 from '../../components/molecules/presentation/templates/template1/images/image 2.png';
import Image3 from '../../components/molecules/presentation/templates/template1/images/image 3.png';
import Image4 from '../../components/molecules/presentation/templates/template1/images/image 4.png';
import Image5 from '../../components/molecules/presentation/templates/template1/images/image 5.png';
import Image6 from '../../components/molecules/presentation/templates/template1/images/image 6.png';
import Image7 from '../../components/molecules/presentation/templates/template1/images/image 7.png';
import Image8 from '../../components/molecules/presentation/templates/template1/images/image 8.png';
import Image9 from '../../components/molecules/presentation/templates/template1/images/image 9.png';
import Image10 from '../../components/molecules/presentation/templates/template1/images/image 10.png';

class Presentation extends Component {
    render(){
        return(
            <div className="contentPresentation">
                <div className="BlocHome1">
                    <div className="colHome">
                        <img className="Logo" src={Logo} alt=""/>
                        <p className="pHome1">
                            Et hastisque hastisque qui pertinax habitus dolorem certamen iram certamen longe longe
                            alacriter longe ductores in scuta tutela bene bene eum certamen pertinax bene certamen
                            securitas in muri pertinax parans iam habitus revocavere proximos terrebat miles locari
                            quorum explicatis occurrere alacriter occurrere parans hastisque cum cunctorum ordinibus
                        </p>
                        <div className="btnPresentation">
                            <Link to="/ressources_relationnelles/about">
                                Présentation
                            </Link>
                            <Link to="/ressources_relationnelles/accueil"><button style={{width:100}} className="btn_presentation buttonInscription">Visiter</button></Link>
                        </div>

                    </div>
                    <div className="colHome3">
                        <img src={ImagePresentation} alt=""/>
                    </div>
                </div>

                <div className="SecteurHome1">
                    <div className="BlocHome">
                        <div className="colHome2">
                            <div className="colonne">
                                <img src={Image1} alt="" style={{width:500}}/>
                            </div>
                            <div className="colonne">
                                <h2 style={{fontWeight:900}}>LOREM  IPSUM</h2>
                                <p className="pHome2">
                                    Et hastisque hastisque qui pertinax habitus dolorem certamen iram certamen longe longe
                                    alacriter longe ductores in scuta tutela bene bene eum certamen pertinax bene certamen
                                    securitas in muri pertinax parans iam habitus revocavere proximos terrebat miles locari
                                    quorum explicatis occurrere alacriter occurrere parans hastisque cum cunctorum ordinibus
                                    distarent parans parans concitat dolorem intempestivum iam rati dolorem anceps certamen
                                    qui cum anceps dolorem gestu terrebat ordinibus terrebat iam in cum scuta tutela miles
                                    poterat sed alacriter et subire in et eum explicatis poterat dolorem hastisque quorum
                                    alacriter tutela consurgentem intempestivum iram explicatis eum iam proximos feriens
                                    certamen rati consurgentem eum parans.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="BlocHome">
                        <div className="colHome2">
                            <div className="colonne">
                                <h2>LOREM  IPSUM</h2>
                                <p className="pHome2">
                                    Et hastisque hastisque qui pertinax habitus dolorem certamen iram certamen longe longe
                                    alacriter longe ductores in scuta tutela bene bene eum certamen pertinax bene certamen
                                    securitas in muri pertinax parans iam habitus revocavere proximos terrebat miles locari
                                    quorum explicatis occurrere alacriter occurrere parans hastisque cum cunctorum ordinibus
                                    distarent parans parans concitat dolorem intempestivum iam rati dolorem anceps certamen
                                    qui cum anceps dolorem gestu terrebat ordinibus terrebat iam in cum scuta tutela miles
                                    poterat sed alacriter et subire in et eum explicatis poterat dolorem hastisque quorum
                                    alacriter tutela consurgentem intempestivum iram explicatis eum iam proximos feriens
                                    certamen rati consurgentem eum parans.
                                </p>
                            </div>
                            <div className="colonne">
                                <img src={Image2} alt="" style={{width:400}}/>
                            </div>
                        </div>
                    </div>
                    <div className="BlocHome">
                        <div className="colonne">
                            <img src={Image3} alt="" />
                        </div>
                        <div className="colonne">
                            <h2>LOREM  IPSUM</h2>
                            <p className="pHome2">
                            Et hastisque hastisque qui pertinax habitus dolorem certamen iram certamen longe longe
                            alacriter longe ductores in scuta tutela bene bene eum certamen pertinax bene certamen
                            securitas in muri pertinax parans iam habitus revocavere proximos terrebat miles locari
                            quorum explicatis occurrere alacriter occurrere parans hastisque cum cunctorum ordinibus
                            distarent parans parans concitat dolorem intempestivum iam rati dolorem anceps certamen
                            qui cum anceps dolorem gestu terrebat ordinibus terrebat iam in cum scuta tutela miles
                            poterat sed alacriter et subire in et eum explicatis poterat dolorem hastisque quorum
                            alacriter tutela consurgentem intempestivum iram explicatis eum iam proximos feriens
                            certamen rati consurgentem eum parans.
                        </p>
                        </div>
                    </div>
                </div>

                <div className="SecteurHome2">
                    <div className="BlocHome3">
                        <img className="imgBloc2" src={Image4} alt="" />
                        <p className="pHome3">
                            Et hastisque hastisque qui pertinax habitus dolorem certamen iram certamen longe longe
                            alacriter longe ductores in scuta tutela bene bene eum certamen pertinax bene certamen
                            securitas in muri pertinax parans iam habitus revocavere proximos terrebat miles locari
                        </p>
                        <button className="buttonInscription" style={{width:100}}>Clique</button>
                    </div>
                    <div className="BlocHome3">
                        <img className="imgBloc2" src={Image5} alt="" />
                        <p className="pHome3">
                            Et hastisque hastisque qui pertinax habitus dolorem certamen iram certamen longe longe
                            alacriter longe ductores in scuta tutela bene bene eum certamen pertinax bene certamen
                            securitas in muri pertinax parans iam habitus revocavere proximos terrebat miles locari
                        </p>
                        <button className="buttonInscription" style={{width:100}}>Clique</button>
                    </div>
                    <div className="BlocHome3">
                        <img className="imgBloc2" src={Image6} alt="" />
                        <p className="pHome3">
                            Et hastisque hastisque qui pertinax habitus dolorem certamen iram certamen longe longe
                            alacriter longe ductores in scuta tutela bene bene eum certamen pertinax bene certamen
                            securitas in muri pertinax parans iam habitus revocavere proximos terrebat miles locari
                        </p>
                        <button className="buttonInscription" style={{width:100}}>Clique</button>
                    </div>
                </div>

                <div className="SecteurHome3">
                    <div className="colonne">
                        <img src={Image7} alt="" style={{width:150}}/>
                        <h2>LOREM IPSUM</h2>
                    </div>
                    <div className="colonne">
                        <img src={Image8} alt="" style={{width:150}}/>
                        <h2>LOREM IPSUM</h2>
                    </div>
                    <div className="colonne">
                        <img src={Image9} alt="" style={{width:150}}/>
                        <h2>LOREM IPSUM</h2>
                    </div>
                    <div className="colonne">
                        <img src={Image10} alt="" style={{width:150}}/>
                        <h2>LOREM IPSUM</h2>
                    </div>
                </div>
            </div>
        )
    }
}
export default Presentation;