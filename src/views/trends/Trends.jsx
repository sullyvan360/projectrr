//*----- MODULES -----//
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { Button } from 'react-bootstrap';

//*----- COMPOSANTS -----//
import ResourceCard from '../../components/organismes/ressourceCard/ResourceCard.jsx';
import Spinner from '../../components/molecules/spinner/spinner'

//CSS
import './Trends.css';

//Icons
import { MdNewReleases } from "react-icons/md";
import { AiOutlineHeart } from "react-icons/ai";
import { RiArticleLine } from "react-icons/ri";
import { BsCameraVideo } from "react-icons/bs";
import { IoGameControllerOutline } from "react-icons/io5";

const Trends = (props) => {
  const url = 'http://directus-ressources-relationnelles.fr.ms-lab.eu/items/rr_ressources?limit=20'
  const [link, setLink] = useState('')
  const [ressources, setRessources] = useState([]);
  const [membres, setMembres] = useState('');
  const [page, setPage] = useState(2);
  const [isFetching, setIsFetching] = useState(false);
  const [isLoading, setIsLoading] = useState(true)

  const loadData = () => {
    axios.get(url + link).then(res => {
      setRessources(res.data.data);
      setIsLoading(false)
    });
    window.scrollTo(0, 0);
  }

  const moreData = () => {
    var datasRessources = url + link + `&page=${page}`;
    axios.get(datasRessources).then(res => {
      setRessources([...ressources, ...res.data.data]);
      setPage(page + 1)
      setIsFetching(false)
    });
  }

  const isScrolling = () => {
    let bottomPage = window.innerHeight + document.documentElement.scrollTop !== document.documentElement.offsetHeight;
    if (bottomPage) {
      return;
    }
    setIsFetching(true)
  }

  const setRessourcesButton = {
    getRessourcesLatest: () => {
      setIsLoading(true)
      axios.get(url + '&filter[DATE_PUBLICATION_RESSOURCE][_gte]=2021-10-10')
        .then(result => {
          setIsLoading(false)
          setLink('&filter[DATE_PUBLICATION_RESSOURCE][_gte]=2021-10-10')
          setRessources(result.data.data)
        });
    },
    getRessourcesMostLike: () => {
      setIsLoading(true)
      axios.get(url + '&filter[NOMBRE_LIKE][_gte]=7000&sort[]=-NOMBRE_LIKE')
        .then(result => {
          setIsLoading(false)
          setLink('&filter[NOMBRE_LIKE][_gte]=7000&sort[]=-NOMBRE_LIKE')
          setRessources(result.data.data)
        });
    },
    getRessourcesArticles: () => {
      setIsLoading(true)
      axios.get(url + '&filter[DESIGNATION_TYPE_RESSOURCE][_eq]=Article')
        .then(result => {
          setIsLoading(false)
          setLink('&filter[DESIGNATION_TYPE_RESSOURCE][_eq]=Article')
          setRessources(result.data.data)
        });
    },
    getRessourcesVideos: () => {
      setIsLoading(true)
      axios.get(url + '&filter[DESIGNATION_TYPE_RESSOURCE][_eq]=Vidéo')
        .then(result => {
          setIsLoading(false)
          setLink('&filter[DESIGNATION_TYPE_RESSOURCE][_eq]=Vidéo')
          setRessources(result.data.data)
        });
    },
    getRessourcesGames: () => {
      setIsLoading(true)
      axios.get(url + '&filter[DESIGNATION_TYPE_RESSOURCE][_eq]=Jeux')
        .then(result => {
          setIsLoading(false)
          setLink('&filter[DESIGNATION_TYPE_RESSOURCE][_eq]=Jeux')
          setRessources(result.data.data)
        });
    },
  }

  useEffect(() => {
    loadData()
    window.addEventListener("scroll", isScrolling);
    return () => window.removeEventListener("scroll", isScrolling);
  }, [])

  useEffect(() => {
    if (isFetching) {
      document.getElementById("parentLoadingData").classList.add("isFetching")
      moreData();
    }
  }, [isFetching]);

  return (
    <>
      <section>
        <h1 className="titleVisitorSitePresentation">Tendances de la communauté</h1>

        <div className="col-12 display-button-tendances">
          <div className="parentButtonTendances">
            <Button onClick={setRessourcesButton.getRessourcesLatest}>Nouveautés<MdNewReleases /></Button>
            <Button onClick={setRessourcesButton.getRessourcesMostLike}>Les plus likés<AiOutlineHeart /></Button>
            <Button onClick={setRessourcesButton.getRessourcesArticles}>Articles<RiArticleLine /></Button>
            <Button onClick={setRessourcesButton.getRessourcesVideos}>Vidéos<BsCameraVideo /></Button>
            <Button onClick={setRessourcesButton.getRessourcesGames}>Jeux<IoGameControllerOutline /></Button>
          </div>
        </div>

        {isLoading ? <Spinner classSpinner="loading-spinner-relative" /> :
          <div className="parent-display-ressources">
            <ul className="display-ressources-list">
              {ressources.map((ressource, i) =>
                <Link key={i} to="/ressources_relationnelles/resourcecontainer">
                  <ResourceCard tableRessources={ressource} tableMembres={membres} />
                </Link>
              )}
            </ul>
            {isFetching ? <Spinner classSpinner="loading-spinner-absolute" /> : ""}
          </div>
        }

      </section>
    </>
  );
}

export default Trends;